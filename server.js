var express = require('express');
var bodyParser = require('body-parser');

var {mongoose} = require('../data/db/mongoose');
var {Ticket} = require('../data/model/schema');
var {TicketPay} = require('../data/model/paySchema');

var app = express();

app.use(bodyParser.json());


// app.post('/ticket_data', (req, res) => {
// 	var ticket = new Ticket({
// 		ticket_status: req.body.ticket_status
// 	});

// 	ticket.save().then(docs => {
// 		res.send(docs);
// 	}, (e) => {
// 		res.status(400).send(e);
	
// 	});
// });


app.get('/ticket_data', (req, res) => {
  Ticket.find({}, {
    "ticket_closed_event": 1,
    "ticket_raised_event": 1
  }).then((tickets) => {
   	 let sum = 0;
     let count = 0;
     for(i=0; i<tickets.length; i++){
     	if(tickets[i].ticket_raised_event!="null" && tickets[i].ticket_closed_event!="null"){
     		var x=new Date(tickets[i].ticket_raised_event.time_stamp);
    		var y=new Date(tickets[i].ticket_closed_event.time_stamp);
     		var timediff = Math.abs(x.getTime()- y.getTime());
     		
     		if(!isNaN(timediff)){
     			//console.log(y-x,i)
     		var diffDays = Math.ceil(timediff / (1000 * 3600 * 24));
     		sum += diffDays;
     		count += 1;
     		}
     	}
    		
    }
    var mean = Math.ceil(sum/count);
    
    res.send({"mean_time":(mean)});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/max_ticket', (req, res) => {
  Ticket.aggregate([
    {
      $group: {
        _id: "$managed_home_id",
        total: {
          $sum: 1
        }
      }
    }, {
      $sort: {
        total: -1
      }
    }
  ]).limit(1).then((tickets) => {
  	var max = tickets[0].total;
  	console.log(max);
    res.send({"max_ticket": (max)});
  }, (e) => {
    res.status(400).send(e);
  });
});

app.get('/ticket_payments', (req, res) => {
   	TicketPay.aggregate(
   	[{$match: {flow:1}},
   	{$group: {_id:"$flow",
   	count: {$sum: 1}}}
   	])

	.then((tickets) => {
		// var count = tickets[0].count;
		// console(count);
    res.send(tickets);
  }, (e) => {
    res.status(400).send(e);
  });
});

app.listen(3000, () => {
	console.log('started on port 3000');
})
module.exports = {app};
